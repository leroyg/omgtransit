# Overview
[OMG Transit](https://omgtransit.com) is a real-time transit aggregation service for the Twin Cities.

## History
OMG Transit was born at a [hackathon](https://medium.com/@omgtransit/the-omg-transit-story-7b02ceb6ea8b) in June 2013 and eventually evolved to support multiple cities.

In an effort to modernize and simplify it's footprint, it was [re-written](https://medium.com/@omgtransit/the-updated-omg-transit-story-2e6a007edd94) in December 2017, and now only supports Minneapolis/St Paul.

It is a progressive web, and can be accessed either via a web browser, or saved to your homescreen. To [Add to Homescreen](https://omgtransit.com/#/apps), use Safari on iOS, or Chrome on Android.

## Usage Guidelines
Our codebase is open source and we encourage merge requests!

### About the app
  * 100% Javascript, built using React and Redux
  * Hosted on Gitlab Pages
  * No back-end

It is easy to get up and running locally. See [Application Setup](#application-setup) below for details.

### Workflow
We manage our task list via [Gitlab Issues](https://gitlab.com/omgtransit/omgtransit/issues).

Please use this if you would like to report a bug or contribute to the codebase.

### [License](LICENSE.md)
We reserve all rights to the OMG Transit name and logo, but our software is available under an MIT license.

## Application Setup

These instructions assume you're on a Mac.

### Prerequisites
  1. node `brew install node`
  2. yarn `brew install yarn`
  3. A [Google Maps API key](https://developers.google.com/maps/documentation/javascript/get-api-key)

### Instructions
  1. Clone the repo
  2. Run `yarn install`
  3. Save your Google Maps API Key to `.env.local`
      * see `.env.local.example` for an example
  4. Run `yarn start` to spin up the app (http://localhost:3000)

## Deploying

OMG Transit is currently live at https://omgtransit.com.

Gitlab Pages automatically deploys with each commit to `master`.

## Testing

We currently use [jest](https://jestjs.io/) and [enzyme](https://airbnb.io/enzyme/) for testing.

`yarn test` will run the whole test suite.


## SSL

CloudFlare is our SSL certificate provider, and the renewal process is automated. In the past, we used Let's Encrypt (via [certbot](https://certbot.eff.org/docs/using.html#manual)) for SSL.

## Data Sources

Transit stop info is provided in a General Transit Feed Specification (GTFS) format. Updating the stop data is currently a manual process.

### Updating Stop Data

* Source: [Metro Transit Schedule Data - General Transit Feed Specification (GTFS)](https://gisdata.mn.gov/dataset/us-mn-state-metc-trans-transit-schedule-google-fd) ([Direct download](ftp://ftp.gisdata.mn.gov/pub/gdrs/data/pub/us_mn_state_metc/trans_transit_schedule_google_fd/csv_trans_transit_schedule_google_fd.zip))
    * Used to populate `src/static/transit_stops.json`
    * Updated weekly
* Instructions
  1. Download and unzip [csv_trans_transit_schedule_google_fd.zip](ftp://ftp.gisdata.mn.gov/pub/gdrs/data/pub/us_mn_state_metc/trans_transit_schedule_google_fd/csv_trans_transit_schedule_google_fd.zip)
  2. Upload `stops.txt` to [Freeformatter](https://www.freeformatter.com/csv-to-json-converter.html) and Convert CSV to JSON
  3. Save output to `src/static/transit_stops.json`
