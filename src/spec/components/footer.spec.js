import React from 'react';
import { shallow } from 'enzyme';
import Footer from '../../components/footer';
import { NavLink } from 'react-router-dom';

import * as helpers from '../../util/helpers';
jest.spyOn(helpers, 'iOSDeviceCheck');

beforeEach(() => {
  helpers.iOSDeviceCheck.mockReturnValue({ iOS: false, iPhoneX: false });
});

it('renders correctly', () => {
  let wrapper = shallow(<Footer />);
  expect(wrapper.find('#footer')).toHaveLength(1);
  expect(wrapper.find('#footer')).not.toHaveClassName('iphonex');
  expect(wrapper.find(NavLink)).toHaveLength(3);
  expect(wrapper.find('span.text')).toHaveLength(3);
});

it('sets iPhoneXClass correctly', () => {
  helpers.iOSDeviceCheck.mockReturnValue({ iOS: true, iPhoneX: true });
  let wrapper = shallow(<Footer />);
  expect(wrapper.find('#footer')).toHaveClassName('iphonex');
});
