import React from 'react';
import { shallow } from 'enzyme';
import App from '../../components/app';
import Header from '../../components/header';
import StatusBar from '../../components/status_bar';
import Footer from '../../components/footer';

it('renders without crashing', () => {
  let wrapper = shallow(<App />);
  expect(wrapper.find('div')).toHaveLength(2);
  expect(wrapper.find(Header)).toHaveLength(1);
  // we don't expect to see StatusBar here because it returns null
  expect(wrapper.find(StatusBar)).toHaveLength(0);
  expect(wrapper.find(Footer)).toHaveLength(1);
});

it('renders children when passed in', () => {
  let child = <div className="child">I am a child</div>;
  let wrapper = shallow(<App>{child}</App>);
  expect(wrapper.find('div')).toHaveLength(3);
  expect(wrapper.find('div.child')).toHaveText('I am a child');
});
