import React from 'react';
import { shallow } from 'enzyme';
import Initializer from '../../components/initializer';
import moment from 'moment';

let initialProps = {
  bikeStations: { isLoading: true },
  userLocation: { isLoading: true },
  loadBikeStations: jest.fn(),
  updateUserLocation: jest.fn()
};

describe('componentDidMount', () => {
  it('updates user location if coords are empty', () => {
    let props = {
      ...initialProps,
      userLocation: {
        isLoading: false,
        coords: {},
        timestamp: moment().subtract(5, 'minutes')
      }
    };
    let wrapper = shallow(<Initializer {...props} />);
    expect(wrapper.updateUserLocation).toBeCalled;
  });

  it('updates user location if timestamp is stale', () => {
    let props = {
      ...initialProps,
      userLocation: {
        isLoading: false,
        coords: { boop: 'beep' },
        timestamp: moment().subtract(12, 'minutes')
      }
    };
    let wrapper = shallow(<Initializer {...props} />);
    expect(wrapper.updateUserLocation).toBeCalled;
  });

  it('does not update user location if coords present and timestamp not stale', () => {
    let props = {
      ...initialProps,
      userLocation: {
        isLoading: false,
        coords: { boop: 'beep' },
        timestamp: moment().subtract(9, 'minutes')
      }
    };
    let wrapper = shallow(<Initializer {...props} />);
    expect(wrapper.updateUserLocation).not.toBeCalled;
  });

  it('updates bike stations if data is empty', () => {
    let props = {
      ...initialProps,
      bikeStations: {
        isLoading: false,
        data: [],
        timestamp: moment().subtract(5, 'minutes')
      }
    };
    let wrapper = shallow(<Initializer {...props} />);
    expect(wrapper.loadBikeStations).toBeCalled;
  });

  it('updates bike stations if timestamp is stale', () => {
    let props = {
      ...initialProps,
      bikeStations: {
        isLoading: false,
        data: [1, 2, 3],
        timestamp: moment().subtract(13, 'hours')
      }
    };
    let wrapper = shallow(<Initializer {...props} />);
    expect(wrapper.loadBikeStations).toBeCalled;
  });

  it('does not update user location if data present and timestamp not stale', () => {
    let props = {
      ...initialProps,
      bikeStations: {
        isLoading: false,
        data: [1, 2, 3],
        timestamp: moment().subtract(11, 'hours')
      }
    };
    let wrapper = shallow(<Initializer {...props} />);
    expect(wrapper.updateUserLocation).not.toBeCalled;
  });
});
