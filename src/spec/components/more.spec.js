import React from 'react';
import { shallow } from 'enzyme';
import More from '../../components/more';
import { NavLink } from 'react-router-dom';

it('renders correctly', () => {
  let wrapper = shallow(<More />);
  expect(wrapper.find('div.more')).toHaveLength(1);
  expect(wrapper.find('div.heading')).toHaveText('More');
  expect(wrapper.find('ul.link-list')).toHaveLength(1);
  expect(wrapper.find('ul.link-list li')).toHaveLength(4);
  expect(wrapper.find('a')).toHaveLength(2);
  expect(wrapper.find(NavLink)).toHaveLength(2);
});
