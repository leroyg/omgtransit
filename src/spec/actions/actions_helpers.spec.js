import React from 'react';
import { shallow } from 'enzyme';
import * as actionHelpers from '../../actions/action_helpers';

describe('convertFavoritesToNewFormat', () => {
  it('converts stops numbers to objects', () => {
    let favorites = [56006, 56027, 15877, 13275, 56038, 51409];

    expect(actionHelpers.convertFavoritesToNewFormat(favorites)).toEqual([
      { stop_id: 56006, stop_type: 'transit' },
      { stop_id: 56027, stop_type: 'transit' },
      { stop_id: 15877, stop_type: 'transit' },
      { stop_id: 13275, stop_type: 'transit' },
      { stop_id: 56038, stop_type: 'transit' },
      { stop_id: 51409, stop_type: 'transit' }
    ]);
  });

  it('filters out null values and strings', () => {
    let favorites = [
      null,
      'boop',
      { stop_id: 56006, stop_type: 'transit' },
      { stop_id: 81, stop_type: 'bike' }
    ];
    expect(actionHelpers.convertFavoritesToNewFormat(favorites)).toEqual([
      { stop_id: 56006, stop_type: 'transit' },
      { stop_id: 81, stop_type: 'bike' }
    ]);
  });

  it('does not change anything if passed only objects', () => {
    let objectFavorites = [{ boop: 'beep', stop_type: 'bike', stop_id: 1234 }];
    expect(actionHelpers.convertFavoritesToNewFormat(objectFavorites)).toEqual(
      objectFavorites
    );
  });
});
