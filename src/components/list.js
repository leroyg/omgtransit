import React, { Component } from 'react';
import StopPreview from '../components/stop_preview';
import Legend from './legend';
import { mplsCoords } from '../static/data';
import moment from 'moment';

class List extends Component {
  componentDidMount() {
    // update user location if no coords or if stale
    if (
      !this.props.coords ||
      moment().diff(moment(this.props.userLocation.timestamp), 'seconds') > 60
    ) {
      this.props.updateUserLocation();
    }

    // load stop data if coords done loading without errors
    if (this.props.coords && !this.props.userLocation.isLoading) {
      this.props.loadNearbyStops(this.props.coords);
    }
  }

  componentWillReceiveProps(nextProps) {
    // only load stop data if location has changed
    if (this.props.coords !== nextProps.coords) {
      this.props.loadNearbyStops(nextProps.coords);
    }
  }

  render() {
    let content;

    // userLocation state/error handling
    if (this.props.userLocation.isLoading) {
      content = (
        <div className="message">
          <h2>Loading your location...</h2>
        </div>
      );
    } else {
      if (this.props.userLocation.error && !this.props.coords) {
        content = (
          <div className="message">
            <span className="heading">Doh!</span>
            <br />
            OMG Transit relies on your geolocation to show nearby transit
            options.<br />
            <br />
            Please allow location access to get the most out of our app!<br />
            <br />
            <a href="" onClick={this.handleDTMplsClick}>
              Click here
            </a>{' '}
            to go straight to Downtown Minneapolis instead.
          </div>
        );
      }
    }

    // nearbyStops loading
    if (
      this.props.nearbyStops.isLoading &&
      !this.props.userLocation.isLoading &&
      !this.props.userLocation.error &&
      !this.props.coords
    ) {
      content = (
        <div className="message">
          <h2>Loading nearby stops...</h2>
        </div>
      );
    }

    // done loading, no error OR coords present
    if (
      !this.props.userLocation.isLoading &&
      !this.props.nearbyStops.isLoading &&
      (!this.props.userLocation.error || this.props.coords)
    ) {
      if (this.props.nearbyStops.data.length === 0) {
        content = (
          <div className="message">
            <span className="heading">Zoinks!</span>
            <br />
            We couldn&#39;t find any stops near your current location.<br />
            <br />
            <a href="" onClick={this.handleDTMplsClick}>
              Click here
            </a>{' '}
            to see Downtown Minneapolis instead.<br />
            <br />
            (This app currently only supports Minneapolis/St Paul.)
          </div>
        );
      } else if (this.props.transitArrivals.error) {
        content = (
          <div className="message">
            <span className="heading">Network error!</span>
            <br />
            Something prevented us from loading real-time data for your nearby
            stops.<br />
            <br />
            If you have an ad blocker like Privacy Badger enabled, please
            disable it for this page.
          </div>
        );
      } else {
        content = (
          <div className="stop-list">
            {this.props.nearbyStops.data
              .map(stop => (
                <StopPreview
                  key={`list-stop-${stop.stop_type}-${stop.stop_id}`}
                  hideNoData={true}
                  {...stop}
                />
              ))
              .slice(0, 10)}
            <Legend />
          </div>
        );
      }
    }

    return <div className="list main-container">{content}</div>;
  }

  handleDTMplsClick = e => {
    e.preventDefault();
    this.props.updateMapCenter(mplsCoords);
  };
}

export default List;
