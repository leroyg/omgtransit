import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import { googleMapsAPIKey } from '../util/helpers';
import MapStopMarker from './map_stop_marker';
import MapStopPreview from './map_stop_preview';
import { mplsCoords } from '../static/data';
import moment from 'moment';
import _isEqual from 'lodash/isEqual';

const YouAreHereMarker = () => <div id="you-are-here" />;
const MapCenterMarker = () => <div id="map-center" />;

class Map extends Component {
  componentDidMount() {
    // update user location if no coords or if stale
    if (
      !this.props.coords ||
      moment().diff(moment(this.props.userLocation.timestamp), 'seconds') > 60
    ) {
      this.props.updateUserLocation();
    }

    this.props.loadNearbyStops(this.props.coords || mplsCoords);
  }

  componentWillUnmount() {
    this.props.toggleMapStopPreview(null);
  }

  createMapOptions(maps) {
    return {
      fullscreenControl: false,
      mapTypeControl: false,
      panControl: false,
      streetViewControl: false,
      styles: [
        {
          featureType: 'poi',
          elementType: 'labels',
          stylers: [{ visibility: 'off' }]
        }
      ],
      zoomControl: true,
      zoomControlOptions: { position: maps.ControlPosition.LEFT_CENTER },
      clickableIcons: false,
      gestureHandling: 'greedy'
    };
  }

  render() {
    const initialCoords = this.shortLatLng(this.props.coords || mplsCoords);

    return (
      <div className="map-container">
        <a
          href=""
          onClick={this.handleUpdateLocationClick}
          alt="update location"
        >
          <div className="map-location-button">
            <i className="fa fa-crosshairs fa-2x" />
          </div>
        </a>

        <GoogleMapReact
          bootstrapURLKeys={{ key: googleMapsAPIKey() }}
          center={initialCoords}
          zoom={16}
          options={this.createMapOptions}
          onChange={this.onMapChange}
          onChildClick={this.handleStopClick}
          onClick={this.handleMapClick}
        >
          {this.props.userLocation.coords && (
            <YouAreHereMarker
              {...this.shortLatLng(this.props.userLocation.coords)}
            />
          )}
          <MapCenterMarker {...initialCoords} />
          {this.props.nearbyStops.data.map(stop => (
            <MapStopMarker
              key={`map-stop-${stop.stop_type}-${stop.stop_id}`}
              lat={stop.stop_lat}
              lng={stop.stop_lon}
              clickable={true}
              active={this.isStopMarkerActive(stop)}
              {...stop}
            />
          ))}
        </GoogleMapReact>
        <MapStopPreview {...this.props.mapStopPreview} />
      </div>
    );
  }

  isStopMarkerActive = stop => {
    return (
      this.props.mapStopPreview &&
      this.props.mapStopPreview.stop_type === stop.stop_type &&
      this.props.mapStopPreview.stop_id === stop.stop_id
    );
  };

  shortLatLng(coords) {
    return { lat: coords.latitude, lng: coords.longitude };
  }

  fullLatLng(coords) {
    return { latitude: coords.lat, longitude: coords.lng };
  }

  onMapChange = params => {
    let newCoords = this.fullLatLng(params.center);
    if (!_isEqual(this.props.coords, newCoords)) {
      this.props.updateMapCenter(newCoords);
      this.props.loadNearbyStops(newCoords);
    }
  };

  handleMapClick = () => {
    this.props.toggleMapStopPreview(null);
  };

  handleStopClick = (key, childProps) => {
    if (childProps.stop_id) {
      this.props.updateMapCenter(this.fullLatLng(childProps));
      this.props.toggleMapStopPreview({ ...childProps });
    }
  };

  handleUpdateLocationClick = e => {
    e.preventDefault();
    this.props.toggleMapStopPreview(null);

    let newCoords = this.props.userLocation.coords || mplsCoords;
    this.props.updateMapCenter(newCoords);

    // only update user location if more than 5 seconds old
    if (
      moment().diff(moment(this.props.userLocation.timestamp), 'seconds') > 5
    ) {
      this.props.updateUserLocation();
    }
  };
}

export default Map;
