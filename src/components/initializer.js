import { Component } from 'react';
import moment from 'moment';
import isEmpty from 'lodash/isEmpty';

class Initializer extends Component {
  componentDidMount() {
    // update user location if stale
    if (
      isEmpty(this.props.userLocation.coords) ||
      moment().diff(moment(this.props.userLocation.timestamp), 'minutes') > 10
    ) {
      this.props.updateUserLocation();
    }

    // update bike stations if stale
    if (
      isEmpty(this.props.bikeStations.data) ||
      moment().diff(moment.unix(this.props.bikeStations.timestamp), 'hours') >
        12
    ) {
      this.props.loadBikeStations();
    }
  }

  render() {
    return null;
  }
}

export default Initializer;
