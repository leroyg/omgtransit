import React, { Component } from 'react';
import MapPreview from './map_preview';
import ArrivalBar from './arrival_bar';
import Legend from './legend';

class TransitStop extends Component {
  componentDidMount() {
    this.props.loadStopInfo('transit', this.props.stop_id);
    this.props.loadStopArrivals(this.props.stop_id);

    // reload every 30 seconds
    this.intervalId = setInterval(() => {
      this.props.loadStopArrivals(this.props.stop_id);
    }, 30 * 1000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  handleFavoriteClick = e => {
    e.preventDefault();
    this.props.updateFavoriteStops({
      stop_type: this.props.stop_type,
      stop_id: this.props.stop_id
    });
  };

  render() {
    let arrivalContent;
    if (this.props.arrivals && this.props.arrivals.length > 0) {
      arrivalContent = this.props.arrivals.map(arrival => (
        <ArrivalBar
          key={`${this.props.stop_id}-${arrival.BlockNumber}-${
            arrival.DepartureTime
          }`}
          {...arrival}
        />
      ));
    } else {
      arrivalContent = (
        <div className={`arrival-bar pnd`}>
          <div className="arrival-info">No Data</div>
        </div>
      );
    }

    return (
      <div className="stop main-container">
        <div className="stop-map-preview">
          <MapPreview {...this.props} />
        </div>
        <div className="heading-bar">
          <div className="stop-heading">{this.props.stop_name}</div>
          <div className="favorite">
            <a
              href=""
              className={`favorite ${this.props.isFavorite && 'gold'}`}
              onClick={this.handleFavoriteClick}
            >
              <i className="fa fa-star fa-lg" />
            </a>
          </div>
        </div>
        <div className="arrivals">{arrivalContent}</div>
        <Legend />
      </div>
    );
  }
}

export default TransitStop;
