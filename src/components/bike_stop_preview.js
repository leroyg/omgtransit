import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { pluralizer } from '../util/helpers';

class BikeStopPreview extends Component {
  componentDidMount() {
    if (!this.props.stop_name) {
      this.props.loadStopInfo('bike', this.props.stop_id);
    }
    this.props.loadBikeAvailability();

    // reload every 30 seconds
    this.intervalId = setInterval(() => {
      this.props.loadBikeAvailability();
    }, 30 * 1000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  handleFavoriteClick = e => {
    e.preventDefault();
    this.props.updateFavoriteStops({
      stop_type: this.props.stop_type,
      stop_id: this.props.stop_id
    });
  };

  render() {
    return (
      <div className="stop-preview">
        <div className="stop-info">
          <Link to={`/stop/niceride/${this.props.stop_id}`}>
            <div className="stop-title">{this.props.stop_name}</div>
            <div className="stop-arrival-chips">
              <span className="arrival-chip pnr real-time">
                {pluralizer(
                  this.props.availability.num_bikes_available || 0,
                  'Bike'
                )}
              </span>
              <span className="arrival-chip p20 real-time">
                {pluralizer(
                  this.props.availability.num_docks_available || 0,
                  'Dock'
                )}
              </span>
            </div>
          </Link>
        </div>
        <div className="favorite">
          <a
            href=""
            className={`favorite ${this.props.isFavorite && 'gold'}`}
            onClick={this.handleFavoriteClick}
          >
            <i className="fa fa-star" />
          </a>
        </div>
      </div>
    );
  }
}

export default BikeStopPreview;
