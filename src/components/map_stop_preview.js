import React from 'react';
import StopPreview from '../components/stop_preview';
import { iOSDeviceCheck } from '../util/helpers';

const MapStopPreview = props => {
  let visible, content;
  const iOSDevice = iOSDeviceCheck();
  const iphoneXClass = iOSDevice.iOS && iOSDevice.iPhoneX ? 'iphonex' : '';

  if (props.stop_id && props.stop_type) {
    visible = 'visible';
    content = (
      <StopPreview
        key={`${props.stop_type}-${props.stop_id}`}
        hideNoData={false}
        {...props}
      />
    );
  }

  return (
    <div id="map-stop-preview" className={`${visible}  ${iphoneXClass}`}>
      {content}
    </div>
  );
};

export default MapStopPreview;
