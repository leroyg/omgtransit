import React, { Component } from 'react';
import MapPreview from './map_preview';
import { pluralizer } from '../util/helpers';
import _isEmpty from 'lodash/isEmpty';

class StopBike extends Component {
  componentDidMount() {
    if (!this.props.stop_name) {
      this.props.loadStopInfo('bike', this.props.stop_id);
    }
    this.props.loadBikeAvailability();

    // reload every 30 seconds
    this.intervalId = setInterval(() => {
      this.props.loadBikeAvailability();
    }, 30 * 1000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  handleFavoriteClick = e => {
    e.preventDefault();
    this.props.updateFavoriteStops({
      stop_type: this.props.stop_type,
      stop_id: this.props.stop_id
    });
  };

  render() {
    let content;
    if (_isEmpty(this.props.availability)) {
      content = (
        <div className="availability-bar pnd">
          <div className="availability-info">No Data</div>
        </div>
      );
    } else {
      content = (
        <div>
          <div className="availability-bar pnr real-time">
            <div className="availability-info">
              {pluralizer(
                this.props.availability.num_bikes_available || 0,
                'Bike'
              ) + ' Available'}
            </div>
          </div>
          <div className="availability-bar p20 real-time">
            <div className="availability-info">
              {pluralizer(
                this.props.availability.num_docks_available || 0,
                'Dock'
              ) + ' Available'}
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className="stop main-container">
        <div className="stop-map-preview">
          <MapPreview {...this.props} />
        </div>
        <div className="heading-bar">
          <div className="stop-heading">{this.props.stop_name}</div>
          <div className="favorite">
            <a
              href=""
              className={`favorite ${this.props.isFavorite && 'gold'}`}
              onClick={this.handleFavoriteClick}
            >
              <i className="fa fa-star fa-lg" />
            </a>
          </div>
        </div>
        <div className="availability">{content}</div>
        <div className="legend">
          For more information about how Nice Ride bike share works,<br />
          please visit{' '}
          <a
            href="https://www.niceridemn.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            niceridemn.org
          </a>
        </div>
      </div>
    );
  }
}

export default StopBike;
