import React, { Component } from 'react';
import statusMessage from '../static/status_message.json';
import { getDisplayMode } from '../util/helpers';
import _includes from 'lodash/includes';
import moment from 'moment';

class StatusBar extends Component {
  componentDidMount() {
    // automatically dismiss after 15 seconds, if visible
    if (this.showStatusBar()) {
      this.timeout = setTimeout(() => {
        this.props.dismissStatusMessage(statusMessage.id);
      }, 15 * 1000);
    }
  }

  componentWillUnmount() {
    clearInterval(this.timeout);
  }

  handleDismissClick = e => {
    e.preventDefault();
    this.props.dismissStatusMessage(statusMessage.id);
  };

  showStatusBar = () => {
    const messageExpired = moment(statusMessage.expiration).isBefore(moment());
    const messageDismissed = _includes(
      this.props.dismissedStatuses,
      statusMessage.id
    );
    const correctDisplayMode = _includes(
      statusMessage.displayModes,
      getDisplayMode()
    );
    return !messageExpired && !messageDismissed && correctDisplayMode;
  };

  render() {
    var messageContent = null;

    if (this.showStatusBar()) {
      messageContent = (
        <div className="status-container">
          <div className="status-bar">
            <div
              className="status-message"
              dangerouslySetInnerHTML={{ __html: statusMessage.messageHtml }}
            />
            <a href="" onClick={this.handleDismissClick} alt="dismiss">
              <div className="status-close">
                <i className="fa fa-close" />
              </div>
            </a>
          </div>
        </div>
      );
    }
    return messageContent;
  }
}

export default StatusBar;
