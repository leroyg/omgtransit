import React from 'react';
import { NavLink } from 'react-router-dom';
import { iOSDeviceCheck } from '../util/helpers';

const Footer = () => {
  const iOSDevice = iOSDeviceCheck();
  const iphoneXClass = iOSDevice.iOS && iOSDevice.iPhoneX ? 'iphonex' : '';

  return (
    <footer id="footer" className={iphoneXClass}>
      <div className="footer-container">
        <NavLink to="/list">
          <i className="fa fa-list" />
          <span className="text">List</span>
        </NavLink>
        <NavLink to="/map">
          <i className="icon-omg-map" />
          <span className="text">Map</span>
        </NavLink>
        <NavLink to="/favorites">
          <i className="fa fa-star" />
          <span className="text">Favorites</span>
        </NavLink>
      </div>
    </footer>
  );
};

export default Footer;
