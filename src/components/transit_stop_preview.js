import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ArrivalChip from './arrival_chip';

class TransitStopPreview extends Component {
  componentDidMount() {
    if (!this.props.stop_name) {
      this.props.loadStopInfo('transit', this.props.stop_id);
    }
    this.props.loadStopArrivals(this.props.stop_id);

    // reload every 30 seconds
    this.intervalId = setInterval(() => {
      this.props.loadStopArrivals(this.props.stop_id);
    }, 30 * 1000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  handleFavoriteClick = e => {
    e.preventDefault();
    this.props.updateFavoriteStops({
      stop_type: this.props.stop_type,
      stop_id: this.props.stop_id
    });
  };

  render() {
    const arrivalData = this.props.arrivals && this.props.arrivals.length > 0;

    if (!arrivalData && this.props.hideNoData) {
      return null;
    }

    let arrivalContent;
    if (arrivalData) {
      arrivalContent = this.props.arrivals
        .slice(0, 4)
        .map(arrival => (
          <ArrivalChip
            key={`${this.props.stop_id}-${arrival.BlockNumber}-${
              arrival.DepartureTime
            }`}
            {...arrival}
          />
        ));
    } else {
      arrivalContent = <span className={`arrival-chip pnd`}>No Data</span>;
    }

    return (
      <div className="stop-preview">
        <div className="stop-info">
          <Link to={`/stop/msp/${this.props.stop_id}`}>
            <div className="stop-title">{this.props.stop_name}</div>
            <div className="stop-arrival-chips">{arrivalContent}</div>
          </Link>
        </div>
        <div className="favorite">
          <a
            href=""
            className={`favorite ${this.props.isFavorite && 'gold'}`}
            onClick={this.handleFavoriteClick}
          >
            <i className="fa fa-star" />
          </a>
        </div>
      </div>
    );
  }
}

export default TransitStopPreview;
