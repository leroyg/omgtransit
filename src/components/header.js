import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { history } from '../store';
import headerLogo from '../assets/images/omg-header-words.svg';

class Header extends Component {
  handleBackButtonClick = e => {
    e.preventDefault();
    history.goBack();
  };

  render() {
    return (
      <header>
        <div className="header-container">
          <a href="" onClick={this.handleBackButtonClick} alt="previous page">
            <i className="fa fa-chevron-left fa-2x back-button" />
          </a>
          <img src={headerLogo} alt="omg-transit" className="header-logo" />
          <NavLink to="/more">
            <i className="fa fa-ellipsis-v fa-2x more-menu" />
          </NavLink>
        </div>
      </header>
    );
  }
}

export default Header;
