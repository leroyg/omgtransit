import React from 'react';
import Initializer from '../containers/initializer';
import Header from '../components/header';
import StatusBar from '../containers/status_bar';
import Footer from './footer';
import '../assets/styles/main.css';

const App = props => (
  <div>
    <Initializer />
    <Header />
    <StatusBar />
    <div className="container">{props.children}</div>
    <Footer />
  </div>
);

export default App;
