// enzyme
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

// enzyme-matchers. see: https://github.com/FormidableLabs/enzyme-matchers
import 'jest-enzyme';

// mock for google analytics
jest.mock('react-ga');

// mock for getDisplayMode() helper method
window.matchMedia =
  window.matchMedia ||
  function() {
    return {
      matches: false,
      addListener: function() {},
      removeListener: function() {}
    };
  };
