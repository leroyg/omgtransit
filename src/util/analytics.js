import ReactGA from 'react-ga';
import { getDisplayMode } from './helpers';

export const initializeTracking = history => {
  ReactGA.initialize('UA-41367617-7');

  const displayMode = getDisplayMode();
  ReactGA.set({ dimension1: displayMode });
  ReactGA.pageview(window.location.pathname + window.location.search);

  history.listen(location => {
    const path = location.pathname + location.search;
    ReactGA.set({ page: path });
    ReactGA.set({ dimension1: displayMode });
    ReactGA.pageview(path);
  });
};

export const trackEvents = store => next => action => {
  let result;
  if (action.type.match(/SUCCESS|FAILURE/)) {
    ReactGA.event({ category: 'Redux Action', action: action.type });
    result = next(action);
  }
  return result;
};
