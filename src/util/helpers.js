export function arrowDirection(direction) {
  return {
    NORTHBOUND: 'up',
    EASTBOUND: 'right',
    SOUTHBOUND: 'down',
    WESTBOUND: 'left'
  }[direction];
}

export function googleMapsAPIKey() {
  // You can append `&v=VERSION` to the api key below to explicitly set a specific
  // version or frequency (default is weekly). We specified `&v=3.31` in the past to
  // mitigate bugs with the new map renderer, but that version has since been sunsetted.
  // Google Maps API Versioning: https://developers.google.com/maps/documentation/javascript/versions
  return process.env.REACT_APP_GOOGLE_MAPS_API_KEY;
}

export function getDisplayMode() {
  const iosStandalone =
    'standalone' in window.navigator && window.navigator.standalone;
  const androidStandalone = window.matchMedia('(display-mode: standalone)')
    .matches;
  return iosStandalone || androidStandalone ? 'standalone' : 'browser';
}

export function iOSDeviceCheck() {
  let [iOS, iPhoneX] = [false, false];

  if (window.navigator.standalone) {
    iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    const ratio = window.devicePixelRatio || 1;
    const screen = {
      width: window.screen.width * ratio,
      height: window.screen.height * ratio
    };
    iPhoneX = iOS && screen.width === 1125 && screen.height === 2436;
  }

  return { iOS: iOS, iPhoneX: iPhoneX };
}

export function pluralizer(num, word) {
  switch (num) {
    case 0:
      return `No ${word}s`;
    case 1:
      return `${num} ${word}`;
    default:
      return `${num} ${word}s`;
  }
}
