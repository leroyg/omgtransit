import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createHashHistory';
import rootReducer from './reducers';
import { loadFromLocalStorage, saveToLocalStorage } from './util/local_storage';
import { initializeTracking, trackEvents } from './util/analytics';
import _throttle from 'lodash/throttle';

export const history = createHistory();
initializeTracking(history);

// use state from local storage if present
const initialState = loadFromLocalStorage('state') || {};

const middleware = applyMiddleware(
  thunk,
  routerMiddleware(history),
  trackEvents
);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  initialState,
  composeEnhancers(middleware)
);

// save state & settings to local storage
store.subscribe(
  _throttle(() => {
    saveToLocalStorage('state', {
      userLocation: store.getState().userLocation,
      userFavorites: store.getState().userFavorites,
      dismissedStatuses: store.getState().dismissedStatuses,
      bikeStations: store.getState().bikeStations
    });
    saveToLocalStorage('settings', {
      lastLocation: history.location.pathname
    });
  }, 1000)
);

export default store;
