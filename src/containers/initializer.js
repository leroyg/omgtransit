import { connect } from 'react-redux';
import InitializerComponent from '../components/initializer';
import { loadBikeStations, updateUserLocation } from '../actions/actions';

const mapStateToProps = state => {
  return {
    bikeStations: state.bikeStations,
    userLocation: state.userLocation
  };
};

const mapDispatchToProps = {
  loadBikeStations,
  updateUserLocation
};

export default connect(mapStateToProps, mapDispatchToProps)(
  InitializerComponent
);
